from pathlib import Path
from setuptools import setup

here = Path(__file__).parent

# Get the long description from the README file
with (here / 'README.rst').open(encoding='utf-8') as f:
    long_description = f.read()

setup(
    name="WinUsbCDC",
    use_git_versioner="gitlab:desc:snapshot",
    description="Python package for communicating with USB / CDC devices on windows via the WinUsb driver",
    long_description=long_description,
    license='MIT',
    author="Andrew Leech",
    author_email="andrew@alelec.net",
    url="https://gitlab.com/alelec/winusbcdc",
    setup_requires=['git-versioner'],
    packages=['winusbcdc'],
)
